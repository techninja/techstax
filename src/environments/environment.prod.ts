export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyC7kGdSt5o4RH6nOItdpqkKWGLNu-yztQY",
    authDomain: "dev-profile-28f95.firebaseapp.com",
    databaseURL: "https://dev-profile-28f95.firebaseio.com",
    projectId: "dev-profile-28f95",
    storageBucket: "gs://dev-profile-28f95.appspot.com",
    messagingSenderId: "116691850732",
    appId: "1:116691850732:web:c3b899705e5bc713a95cc9",
    measurementId: "G-HPFWS15E4Z"
  }
};