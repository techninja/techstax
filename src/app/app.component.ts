import { Component, OnInit } from '@angular/core';
import { NbThemeService, NbIconLibraries } from '@nebular/theme';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  isDarkMode: boolean = false;

  constructor(private themeService: NbThemeService, private iconLibraries: NbIconLibraries) {}
  ngOnInit(): void {
    this.iconLibraries.registerFontPack('font-awesome', { packClass: 'fa', iconClassPrefix: 'fa' });
    this.iconLibraries.registerFontPack('regular', { packClass: 'far', iconClassPrefix: 'fa' });
    this.iconLibraries.registerFontPack('solid', { packClass: 'fas', iconClassPrefix: 'fa' });
  }

  switchTheme() {
    if (!this.isDarkMode) this.themeService.changeTheme('dark');
    else this.themeService.changeTheme('default');
    this.isDarkMode = !this.isDarkMode;
  }
}
