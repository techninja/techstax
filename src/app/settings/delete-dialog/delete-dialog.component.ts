import { AuthService } from './../../_services/auth.service';
import { take } from 'rxjs/operators';
import { IUser } from './../../_interfaces/user';
import { FirebaseService } from './../../_services/firebase.service';
import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { Profile } from 'src/app/_interfaces/profile';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss'],
})
export class DeleteDialogComponent implements OnInit {
  constructor(
    protected ref: NbDialogRef<DeleteDialogComponent>,
    private firebaseService: FirebaseService,
    public authService: AuthService
  ) {}

  ngOnInit() {}

  cancel() {
    this.ref.close();
  }

  delete(user: IUser) {
    //only delete user-reference from profile --> user still exists in db
    this.firebaseService
      .queryProfileByUserUid(user.uid)
      .pipe(take(1))
      .subscribe((profiles: Profile[]) => {
        let delProfile: Profile = profiles[0];
        delProfile.userDocId = '';
        delProfile.uniqueName = 'del34590t';
        this.firebaseService.updateProfile(delProfile).then(() => {
          this.authService.signOut();
          this.ref.close();
        });
      });
  }
}
