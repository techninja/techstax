import { NewsletterDialogComponent } from './newsletter-dialog/newsletter-dialog.component';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog.component';
import { FirebaseService } from './../_services/firebase.service';
import { Profile } from './../_interfaces/profile';
import { Router } from '@angular/router';
import { AuthService } from './../_services/auth.service';
import { Component, OnInit } from '@angular/core';
import { NbMenuService, NbDialogService } from '@nebular/theme';
import { filter, map, switchMap, take } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
  userMenu = [{ title: 'Profile' }, { title: 'Settings' }, { title: 'Log out' }];
  profiles$: Observable<Profile[]>;

  constructor(
    public authService: AuthService,
    private nbMenuService: NbMenuService,
    private router: Router,
    private firebaseServcie: FirebaseService,
    private dialogService: NbDialogService
  ) {}

  ngOnInit() {
    this.profiles$ = this.authService.user$.pipe(
      switchMap((user) => {
        return this.firebaseServcie.queryProfileByUserUid(user.uid);
      })
    );

    //handle context menu item click
    this.nbMenuService
      .onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'my-context-menu'),
        map(({ item: { title } }) => title)
      )
      .subscribe((title) => {
        if (title === 'Log out') this.authService.signOut();
        else if (title === 'Settings') {
          this.router.navigateByUrl('settings');
        } else if (title === 'Profile') {
          this.authService.user$.pipe(take(1)).subscribe((user) => {
            this.firebaseServcie
              .queryProfileByUserUid(user.uid)
              .pipe(take(1))
              .subscribe((profiles) => {
                this.router.navigateByUrl(profiles[0].uniqueName);
              });
          });
        }
      });
  }

  openDeleteDialog(profile: Profile) {
    this.dialogService.open(DeleteDialogComponent, {
      autoFocus: false,
      closeOnBackdropClick: true,
    });
  }

  openNewsletterDialog() {
    this.dialogService.open(NewsletterDialogComponent, {
      autoFocus: false,
      closeOnBackdropClick: true,
    });
  }
}
