import { WorkExperience } from './../../_interfaces/work-experience';
import { Component, OnInit, Input } from '@angular/core';
import { Profile } from 'src/app/_interfaces/profile';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbDialogRef } from '@nebular/theme';
import { ProfileDialogComponent } from '../profile-dialog/profile-dialog.component';
import { FirebaseService } from 'src/app/_services/firebase.service';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-work-dialog',
  templateUrl: './work-dialog.component.html',
  styleUrls: ['./work-dialog.component.scss'],
})
export class WorkDialogComponent implements OnInit {
  @Input() profile: Profile;

  firstForm: FormGroup;
  workExperiences: WorkExperience[];
  isCurrent: boolean = false;
  unsubscribe$: Subject<void> = new Subject<void>();
  companyLogoUrl: string = '';

  constructor(
    private fb: FormBuilder,
    protected ref: NbDialogRef<ProfileDialogComponent>,
    private firebaseService: FirebaseService,
    private http: HttpClient
  ) {}

  ngOnInit() {
    this.firstForm = this.fb.group({
      companyName: ['', [Validators.required]],
      position: ['', [Validators.required]],
      description: ['', [Validators.required]],
      startDate: ['', [Validators.required]],
      endDate: [],
      isCurrent: [],
    });

    this.workExperiences = [];
    if (!this.profile.workExperiences) this.profile.workExperiences = [];
    else this.workExperiences = JSON.parse(JSON.stringify(this.profile.workExperiences));
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  cancel() {
    this.ref.close();
  }

  addWork() {
    if (!this.firstForm.valid) window.alert('Please fill out all required input fields!');
    else {
      if (!this.isCurrent && !this.firstForm.value.endDate) window.alert('Please fill out all required input fields!');
      else {
        let exp: WorkExperience = {
          companyName: this.firstForm.value.companyName,
          position: this.firstForm.value.position,
          description: this.firstForm.value.description,
          startDateSeconds: this.firstForm.value.startDate.getTime(),
          isCurrent: this.isCurrent,
        };
        if (!this.isCurrent) exp.endDateSeconds = this.firstForm.value.endDate.getTime();

        this.firstForm.reset();
        this.toggleCheckbox(false);
        this.workExperiences.push(exp);

        //sort: 1. sort end date descending - 2. if is current sort start date descending
        this.workExperiences = [...this.workExperiences].sort((a, b) => {
          if (a.isCurrent && b.isCurrent)
            return new Date(b.startDateSeconds).valueOf() - new Date(a.startDateSeconds).valueOf();
          else if (a.isCurrent && !b.isCurrent) return -1;
          else if (!a.isCurrent && b.isCurrent) return 1;
          else return new Date(b.endDateSeconds).valueOf() - new Date(a.endDateSeconds).valueOf();
        });
      }
    }
  }

  toggleCheckbox($event) {
    this.isCurrent = $event;
  }

  deleteWork(work: WorkExperience) {
    for (let i = 0; i < this.workExperiences.length; i++) {
      const w = this.workExperiences[i];
      if (work === w) this.workExperiences.splice(i, 1);
    }
  }

  saveWorkExperiences() {
    this.profile.workExperiences = this.workExperiences;
    this.firebaseService.updateProfile(this.profile);
    this.ref.close();
  }
}
