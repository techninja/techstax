import { Image } from './../../../_interfaces/image';
import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'app-image-dialog',
  templateUrl: './image-dialog.component.html',
  styleUrls: ['./image-dialog.component.scss'],
})
export class ImageDialogComponent implements OnInit {
  @Input() image: Image;
  imageIsLoaded: boolean = false;

  constructor(protected ref: NbDialogRef<ImageDialogComponent>) {}

  ngOnInit() {}

  imageLoaded() {
    this.imageIsLoaded = true;
  }

  closeDialog() {
    this.ref.close();
  }
}
