import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechStackDialogComponent } from './tech-stack-dialog.component';

describe('TechStackDialogComponent', () => {
  let component: TechStackDialogComponent;
  let fixture: ComponentFixture<TechStackDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechStackDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechStackDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
