import { FirebaseService } from './../../_services/firebase.service';
import { NbDialogRef } from '@nebular/theme';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ProfileDialogComponent } from '../profile-dialog/profile-dialog.component';
import { Technology } from 'src/app/_interfaces/technology';
import { Profile } from 'src/app/_interfaces/profile';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { icons } from '../../_interfaces/devicon';

@Component({
  selector: 'app-tech-stack-dialog',
  templateUrl: './tech-stack-dialog.component.html',
  styleUrls: ['./tech-stack-dialog.component.scss'],
})
export class TechStackDialogComponent implements OnInit {
  @Input() profile: Profile;
  @ViewChild('autoCompleteInput', { read: MatAutocompleteTrigger }) autoComplete: MatAutocompleteTrigger; //used for scroll event to close it

  primaryTechnologies: Technology[];
  secondaryTechnologies: Technology[];

  techCtrl = new FormControl();
  filteredTechs: Observable<string[]>;
  previewTechs: string[] = [];

  constructor(protected ref: NbDialogRef<ProfileDialogComponent>, private firebaseService: FirebaseService) {
    //if autocomplete input is empty, don't display techs
    this.techCtrl.valueChanges.subscribe((tech) => {
      if (this.techCtrl.value && this.techCtrl.value.length > 0) {
        this.setIconArray();
      } else {
        this.previewTechs = [];
      }
    });
    //filter techs on change
    this.filteredTechs = this.techCtrl.valueChanges.pipe(
      map((tech) => (tech ? this._filterTechs(tech) : this.previewTechs.slice()))
    );
  }

  ngOnInit() {
    window.addEventListener('scroll', this.scrollEvent, true);

    this.primaryTechnologies = [];
    this.secondaryTechnologies = [];

    if (!this.profile.primaryTechnologies) this.profile.primaryTechnologies = [];
    else {
      this.primaryTechnologies = JSON.parse(JSON.stringify(this.profile.primaryTechnologies)); //clone array to avoid reference
    }
    if (!this.profile.secondaryTechnologies) this.profile.secondaryTechnologies = [];
    else this.secondaryTechnologies = JSON.parse(JSON.stringify(this.profile.secondaryTechnologies)); //clone array to avoid reference
  }
  private _filterTechs(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.previewTechs.filter((tech) => tech.toLowerCase().indexOf(filterValue) === 0);
  }

  setIconArray() {
    this.previewTechs = [];
    for (let key in icons) {
      this.previewTechs.push(icons[key].displayName);
    }
  }
  getIcon(inputName: string): string {
    if (icons[inputName.toLowerCase()]) return icons[inputName.toLowerCase()].iconName;
  }

  //if user scrolls inside the dialog, the autocomplete field will close
  scrollEvent = (event: any): void => {
    if (this.autoComplete.panelOpen) this.autoComplete.closePanel();
  };

  cancel() {
    this.ref.close();
  }

  addTechnology(inputTech?: string) {
    let value;
    if (inputTech) value = inputTech;
    else value = this.techCtrl.value;

    if (value && value != '') {
      let alreadyExists: boolean = false;

      for (let tech of this.secondaryTechnologies) {
        if (tech.name.toLowerCase() == value.toLowerCase()) {
          alreadyExists = true;
          window.alert('Technology already added!');
          break;
        }
      }
      if (!alreadyExists) {
        let tech: Technology = {
          name: value,
          level: 1,
          isFavourite: true,
        };

        tech.isFavourite = false;
        this.secondaryTechnologies.push(tech);
      }
      this.previewTechs = [];
      this.techCtrl.reset();
    } else window.alert('Please enter a technology!');
  }

  onTechChange(tech: Technology) {}

  onPositionChangePrimary(tech: Technology) {
    let oldPos: number;
    //find element index
    for (let i = 0; i < this.primaryTechnologies.length; i++) {
      const pri = this.primaryTechnologies[i];
      if (tech === pri) {
        oldPos = i;
        break;
      }
    }
    if (oldPos != 0) {
      //swap elements
      let oldElement = this.primaryTechnologies[oldPos - 1];
      this.primaryTechnologies[oldPos] = oldElement;
      this.primaryTechnologies[oldPos - 1] = tech;
    }
  }

  onPositionChangeSecondary(tech: Technology) {
    let oldPos: number;
    //find element index
    for (let i = 0; i < this.secondaryTechnologies.length; i++) {
      const sec = this.secondaryTechnologies[i];
      if (tech === sec) {
        oldPos = i;
        break;
      }
    }
    if (oldPos != 0) {
      //swap elements
      let oldElement = this.secondaryTechnologies[oldPos - 1];
      this.secondaryTechnologies[oldPos] = oldElement;
      this.secondaryTechnologies[oldPos - 1] = tech;
    }
  }

  onTechDelete(tech: Technology) {
    for (let i = 0; i < this.primaryTechnologies.length; i++) {
      const primaryTech = this.primaryTechnologies[i];
      if (tech === primaryTech) this.primaryTechnologies.splice(i, 1);
    }
  }

  onTechDeleteSecondary(tech: Technology) {
    for (let i = 0; i < this.secondaryTechnologies.length; i++) {
      const secondaryTech = this.secondaryTechnologies[i];
      if (tech === secondaryTech) this.secondaryTechnologies.splice(i, 1);
    }
  }

  onFavouriteChange(tech: Technology) {
    if (tech.isFavourite && this.primaryTechnologies.length < 5) {
      this.primaryTechnologies.push(tech);
      for (let i = 0; i < this.secondaryTechnologies.length; i++) {
        const sec = this.secondaryTechnologies[i];
        if (tech === sec) {
          this.secondaryTechnologies.splice(i, 1);
          break;
        }
      }
    } else if (!tech.isFavourite) {
      for (let i = 0; i < this.primaryTechnologies.length; i++) {
        const pri = this.primaryTechnologies[i];
        if (tech === pri) {
          this.primaryTechnologies.splice(i, 1);
          break;
        }
      }
      this.secondaryTechnologies.push(tech);
    } else if (tech.isFavourite) {
      window.alert('Maximum number of top technologies reached!');
      tech.isFavourite = false;
    }
  }

  saveTechStack() {
    this.profile.primaryTechnologies = this.primaryTechnologies;
    this.profile.secondaryTechnologies = this.secondaryTechnologies;
    this.firebaseService.updateProfile(this.profile);
    this.ref.close();
  }
}
