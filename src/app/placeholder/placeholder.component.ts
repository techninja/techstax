import { FirebaseService } from './../_services/firebase.service';
import { AuthService } from './../_services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { take } from 'rxjs/operators';

const mobileBreakpoint: number = 992;

@Component({
  selector: 'app-placeholder',
  templateUrl: './placeholder.component.html',
  styleUrls: ['./placeholder.component.scss'],
})
export class PlaceholderComponent implements OnInit {
  showLandingPage: boolean = false;
  isMobile: boolean = false;
  firstForm: FormGroup;

  constructor(
    private router: Router,
    private authService: AuthService,
    private route: ActivatedRoute,
    private firebaseService: FirebaseService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    //if route is changed to landing page
    this.route.params.subscribe(() => {
      this.firstForm = this.fb.group({
        email: ['', [Validators.required, Validators.email]],
      });

      this.isMobile = this.checkWidth();
      this.authService.user$.subscribe((user) => {
        if (user) {
          this.firebaseService
            .queryProfileByUserUid(user.uid)
            .pipe(take(1))
            .subscribe((profiles) => {
              if (profiles[0]) this.router.navigate([profiles[0].uniqueName]);
            });
        } else this.showLandingPage = true; //only show landing page if user isnt logged in
      });
    });
  }

  scrollToFragment(fragment: string) {
    const element = document.querySelector(fragment);
    if (element) element.scrollIntoView({ behavior: 'smooth', block: 'start' });
  }

  //listen to window resize
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isMobile = this.checkWidth();
  }

  checkWidth(): boolean {
    if (window.innerWidth <= mobileBreakpoint) return true;
    else return false;
  }

  navigateSampleProfile() {
    this.router.navigateByUrl('johndoe');
  }
}
