import { Technology } from '../_interfaces/technology';
import { FirebaseService } from '../_services/firebase.service';
import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../_services/auth.service';
import { IUser } from '../_interfaces/user';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class CreateComponent implements OnInit {
  innerWidth: any; //screen size
  mobileSize: number = 768;

  firstForm: FormGroup;
  secondForm: FormGroup;
  thirdForm: FormGroup;
  fourthForm: FormGroup;

  stepperLabel1: string = 'General Information';
  stepperLabel2: string = 'Primary Tech Stack';
  stepperLabel3: string = 'Further Technologies';
  stepperLabel4: string = 'Recent Employers';

  primaryTechnologies: Technology[];
  secondaryTechnologies: string[];

  user: IUser;

  constructor(private authService: AuthService, private firebaseService: FirebaseService, private fb: FormBuilder) {
    let testTech: Technology = { name: 'test', level: 1, isFavourite: true };

    this.primaryTechnologies = [testTech];
    this.secondaryTechnologies = [];
  }

  ngOnInit() {
    this.setScreenSize(window.innerWidth); //get current screen width

    this.authService.user$.subscribe((user) => {
      this.user = user;
    });

    this.firstForm = this.fb.group({
      uniqueName: [],
    });

    this.secondForm = new FormGroup({
      name: new FormControl(''),
    });

    this.thirdForm = new FormGroup({
      tech: new FormControl(''),
    });

    this.fourthForm = new FormGroup({
      employer: new FormControl(''),
    });
  }

  @HostListener('window:resize', ['$event']) //get current screen size when changed
  onResize(event) {
    this.setScreenSize(window.innerWidth);
  }

  setScreenSize(size) {
    this.innerWidth = size;
    if (size < this.mobileSize) {
      this.stepperLabel1 = '';
      this.stepperLabel2 = '';
      this.stepperLabel3 = '';
      this.stepperLabel4 = '';
    } else {
      this.stepperLabel1 = 'General Information';
      this.stepperLabel2 = 'Primary Tech Stack';
      this.stepperLabel3 = 'Further Technologies';
      this.stepperLabel4 = 'Recent Employers';
    }
  }

  addPrimaryTechnology() {
    let tech: Technology = {
      name: this.secondForm.value.name,
      level: 1,
      isFavourite: true,
    };
    this.primaryTechnologies.push(tech);
  }

  onTechChange(tech: Technology) {
    console.log(tech);
  }

  onTechDelete(tech: Technology) {
    console.log(tech);
  }

  addSecondaryTechnology() {
    this.secondaryTechnologies.push(this.secondForm.value.tech);
  }

  removeSecondaryTechnology(tech: string) {
    console.log(tech);
  }

  onFirstSubmit() {
    this.firstForm.markAsDirty();
    if (!this.firstForm.valid) {
      console.log('form invalid');
      return;
    }
  }

  onSecondSubmit() {
    this.secondForm.markAsDirty();
  }

  onThirdSubmit() {
    this.thirdForm.markAsDirty();
  }
  onFourthSubmit() {
    this.thirdForm.markAsDirty();
  }
}
