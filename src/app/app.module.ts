import { UploadTaskComponent } from './_components/upload-task/upload-task.component';
import { AuthService } from './_services/auth.service';
import { CreateComponent } from './create/create.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  NbThemeModule,
  NbLayoutModule,
  NbButtonModule,
  NbCardModule,
  NbStepperModule,
  NbCheckboxModule,
  NbProgressBarModule,
  NbIconModule,
  NbDialogModule,
  NbUserModule,
  NbContextMenuModule,
  NbMenuService,
  NbMenuModule,
  NbAccordionModule,
  NbDatepickerModule,
  NbSelectModule,
  NbInputModule,
  NbPopoverModule,
} from '@nebular/theme';
import { RouterModule } from '@angular/router';
import { FirebaseService } from './_services/firebase.service';
import { AuthGuard } from './_services/auth.guard';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { NotFoundComponent } from './not-found/not-found.component';
import { PlaceholderComponent } from './placeholder/placeholder.component';
import { AddDialogComponent } from './home/add-dialog/add-dialog.component';
import { ProjectDialogComponent } from './home/project-dialog/project-dialog.component';
import { ProfileDialogComponent } from './home/profile-dialog/profile-dialog.component';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { DropzoneDirective } from './_directives/drop-zone.directive';
import { TechStackDialogComponent } from './home/tech-stack-dialog/tech-stack-dialog.component';
import { WorkDialogComponent } from './home/work-dialog/work-dialog.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { TechCardComponent } from './home/tech-stack-dialog/tech-card/tech-card.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FooterComponent } from './_components/footer/footer.component';
import { NewsletterComponent } from './_components/newsletter/newsletter.component';
import { AngularFireAnalyticsModule, ScreenTrackingService, UserTrackingService } from '@angular/fire/analytics';
import { MarkdownModule } from 'ngx-markdown';
import { SafePipe } from './_pipes/safe.pipe';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { SettingsComponent } from './settings/settings.component';
import { NewsletterDialogComponent } from './settings/newsletter-dialog/newsletter-dialog.component';
import { DeleteDialogComponent } from './settings/delete-dialog/delete-dialog.component';
import { ImageDialogComponent } from './home/project-dialog/image-dialog/image-dialog.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    CreateComponent,
    TechCardComponent,
    ProfileDialogComponent,
    NotFoundComponent,
    PlaceholderComponent,
    AddDialogComponent,
    ProjectDialogComponent,
    DropzoneDirective,
    UploadTaskComponent,
    TechStackDialogComponent,
    WorkDialogComponent,
    FooterComponent,
    NewsletterComponent,
    SafePipe,
    SettingsComponent,
    NewsletterDialogComponent,
    DeleteDialogComponent,
    ImageDialogComponent,
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAnalyticsModule,
    AngularFirestoreModule,
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'default' }),
    NbLayoutModule,
    RouterModule,
    NbButtonModule,
    ReactiveFormsModule,
    NbCardModule,
    NbStepperModule,
    HttpClientModule,
    NbCheckboxModule,
    NbProgressBarModule,
    NbIconModule,
    NbEvaIconsModule,
    AngularFireStorageModule,
    NbDialogModule.forRoot(),
    FormsModule,
    NbUserModule,
    NbContextMenuModule,
    NbMenuModule.forRoot(),
    NbAccordionModule,
    NbDatepickerModule.forRoot(),
    DragDropModule,
    MDBBootstrapModule.forRoot(),
    NbSelectModule,
    NbInputModule,
    MarkdownModule.forRoot(),
    ClipboardModule,
    NbPopoverModule,
    MatAutocompleteModule,
  ],
  providers: [FirebaseService, AuthGuard, AuthService, NbMenuService, ScreenTrackingService, UserTrackingService],
  bootstrap: [AppComponent],
  entryComponents: [
    ProjectDialogComponent,
    AddDialogComponent,
    ProfileDialogComponent,
    TechStackDialogComponent,
    WorkDialogComponent,
    NewsletterComponent,
    ImageDialogComponent,
  ],
})
export class AppModule {}
