import { Injectable } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import { IUser } from '../_interfaces/user';
import { Profile } from '../_interfaces/profile';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
  constructor(private firestore: AngularFirestore, private auth: AuthService) {}

  form = new FormGroup({
    customerName: new FormControl(''),
    orderNumber: new FormControl(''),
    completed: new FormControl(false),
  });

  updateUser(user: IUser) {
    return this.firestore.collection('users').doc(user.uid).set(user, { merge: true }); //merge true: replace provided values instead of whole document
  }

  queryUserByUid(uid): Observable<IUser> {
    return this.firestore.doc<IUser>('users/' + uid).valueChanges();
  }

  createProfile(profile: Profile): Promise<DocumentReference> {
    return this.firestore.collection('profiles').add(profile);
  }

  updateProfile(profile: Profile): Promise<void> {
    return this.firestore.collection('profiles').doc(profile.id).set(profile, { merge: true }); //merge true: replace provided values instead of whole document
  }

  queryProfileByUserUid(uid: string): Observable<Profile[]> {
    return this.firestore
      .collection<Profile>('profiles', (ref) => ref.where('userDocId', '==', uid))
      .valueChanges({ idField: 'eventId' });
  }

  queryProfileByUniqueName(uniqueName: string): Observable<Profile[]> {
    return this.firestore
      .collection<Profile>('profiles', (ref) => ref.where('uniqueName', '==', uniqueName))
      .valueChanges();
  }

  queryNewsletterEmail(email: string): Observable<any[]> {
    return this.firestore.collection('newsletter', (ref) => ref.where('email', '==', email)).valueChanges();
  }

  createNewsletterMail(email: string, isFeature: boolean): Promise<DocumentReference> {
    return this.firestore.collection('newsletter').add({ email: email, isFeature: isFeature });
  }

  deleteUser(user: IUser) {
    return this.firestore.doc<IUser>('users/' + user.uid).delete();
  }
}
