import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { IUser } from '../_interfaces/user';

@Injectable({ providedIn: 'root' })
export class AuthService {
  user$: Observable<IUser>;

  constructor(private afAuth: AngularFireAuth, private afs: AngularFirestore, private router: Router) {
    this.user$ = this.afAuth.authState.pipe(
      switchMap((user) => {
        // Logged in
        if (user) {
          return this.afs.doc<IUser>(`users/${user.uid}`).valueChanges();
        } else {
          // Logged out
          return of(null);
        }
      })
    );
  }

  async googleSignin() {
    let isError: boolean = false;
    const provider = new auth.GoogleAuthProvider();
    const credential = await this.afAuth
      .signInWithPopup(provider)
      .then((res) => {
        return res;
      })
      .catch((error) => {
        console.log(error);
        window.alert(error);
        isError = true;
        return error;
      });
    if (!isError) return this.updateUserOnLogin(credential.user);
  }

  async githubSignin() {
    let isError: boolean = false;
    const provider = new auth.GithubAuthProvider();
    const credential = await this.afAuth
      .signInWithPopup(provider)
      .then((res) => {
        return res;
      })
      .catch((error) => {
        console.log(error);
        window.alert(error);
        isError = true;
        return error;
      });
    if (!isError) return this.updateUserOnLogin(credential.user);
  }

  private updateUserOnLogin(user: IUser) {
    // Sets user data to firestore on login
    const userRef: AngularFirestoreDocument<IUser> = this.afs.doc(`users/${user.uid}`);

    let tmpDisplayName: string;
    if (!user.displayName) tmpDisplayName = user.email;
    else tmpDisplayName = user.displayName;

    const data = {
      uid: user.uid,
      email: user.email,
      displayName: tmpDisplayName,
      photoURL: user.photoURL,
    };
    return userRef.set(data, { merge: true });
  }

  async signOut() {
    await this.afAuth.signOut().then(() => {
      window.location.reload();
    });
  }
}
