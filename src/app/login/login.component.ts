import { FirebaseService } from './../_services/firebase.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AuthService } from '../_services/auth.service';
import { Profile } from '../_interfaces/profile';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  error: string = '';
  form = new FormGroup({});

  googleLoading: boolean = false;
  githubLoading: boolean = false;

  constructor(
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private firebaseService: FirebaseService
  ) {}

  ngOnInit() {}

  loginGithub() {
    this.githubLoading = true;
    this.auth.githubSignin().then(() => {
      this.githubLoading = false;
      this.handleLogin();
    });
  }

  loginGoogle() {
    this.googleLoading = true;
    this.auth.googleSignin().then(() => {
      this.googleLoading = false;
      this.handleLogin();
    });
  }

  handleLogin() {
    this.auth.user$.subscribe((user) => {
      if (user) {
        this.firebaseService
          .queryProfileByUserUid(user.uid)
          .pipe(take(1))
          .subscribe((profile) => {
            if (profile[0]) {
              //profile already exists
              let returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

              //no returnurl specified
              if (returnUrl === '/') this.router.navigateByUrl('/' + profile[0].uniqueName.toLowerCase());
              else this.router.navigateByUrl(returnUrl);
            } else {
              //create profile
              let profile: Profile = {
                id: null,
                uniqueName: user.uid.toLowerCase(),
                displayName: user.displayName,
                currentRole: null,
                isRemote: true,
                userDocId: user.uid,
                projects: [],
                profilePicturePath:
                  'https://firebasestorage.googleapis.com/v0/b/dev-profile-28f95.appspot.com/o/default%2Favatar.png?alt=media&token=9e127a1e-e2b7-4114-97e2-b6a4143ab1a2',
                profileBackgroundPath:
                  'https://firebasestorage.googleapis.com/v0/b/dev-profile-28f95.appspot.com/o/default%2Fbackground_arch.jpg?alt=media&token=a99fe628-1e11-401e-bfa0-ce570c4e1062',
              };
              this.firebaseService.createProfile(profile).then((res) => {
                profile.id = res.id;
                //update profile doc id
                this.firebaseService.updateProfile(profile).then(() => {
                  this.router.navigateByUrl('/' + user.uid.toLowerCase());
                });
              });
            }
          });
      }
    });
  }
}
