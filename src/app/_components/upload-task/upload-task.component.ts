import { IUser } from '../../_interfaces/user';
import { Image } from '../../_interfaces/image';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'upload-task',
  templateUrl: './upload-task.component.html',
  styleUrls: ['./upload-task.component.scss'],
})
export class UploadTaskComponent implements OnInit {
  @Output() fileEvent = new EventEmitter<File>();
  @Output() imageEvent = new EventEmitter<Image>();
  @Input() file: File;
  @Input() user: IUser;
  @Input() isProjectView: boolean = false;

  task: AngularFireUploadTask;

  percentage: Observable<number>;
  snapshot: Observable<any>;
  downloadURL: string;

  constructor(private storage: AngularFireStorage, private db: AngularFirestore) {}

  ngOnInit() {
    this.startUpload();
  }

  startUpload() {
    // The storage path
    // const path = `projects/${Date.now()}_${this.file.name}`;
    const path = 'users/' + this.user.uid + '/' + Date.now() + '_' + this.file.name;

    // Reference to storage bucket
    const ref = this.storage.ref(path);

    // The main task
    this.task = this.storage.upload(path, this.file);

    // Progress monitoring
    this.percentage = this.task.percentageChanges();

    this.snapshot = this.task.snapshotChanges().pipe(
      // tap(console.log),
      // The file's download URL
      finalize(async () => {
        this.downloadURL = await ref.getDownloadURL().toPromise();
        let image: Image = {
          name: this.file.name,
          description: '',
          url: this.downloadURL,
        };
        this.imageEvent.emit(image);
      })
    );
  }

  isActive(snapshot) {
    return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes;
  }

  remove() {
    this.storage.storage.refFromURL(this.downloadURL).delete();
    this.fileEvent.emit(this.file);
  }

  get status() {
    return 'success';
  }

  get canIncrease() {
    return true;
  }

  get canDecrease() {
    return true;
  }
}
