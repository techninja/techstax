import { FirebaseService } from 'src/app/_services/firebase.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/_services/auth.service';
import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { AddDialogComponent } from 'src/app/home/add-dialog/add-dialog.component';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.scss'],
})
export class NewsletterComponent implements OnInit {
  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private firebaseService: FirebaseService,
    protected ref: NbDialogRef<AddDialogComponent>
  ) {}

  firstForm: FormGroup;

  ngOnInit() {
    this.firstForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
    });

    this.authService.user$.subscribe((res) => {
      if (res) this.firstForm.controls.email.setValue(res.email);
    });
  }

  subscribe() {
    if (this.firstForm.valid) {
      this.firebaseService
        .queryNewsletterEmail(this.firstForm.value.email)
        .pipe(take(1))
        .subscribe((res) => {
          if (res.length === 0) {
            this.firebaseService.createNewsletterMail(this.firstForm.value.email, true).then(() => this.ref.close());
          } else this.ref.close();
        });
    } else window.alert('Please fill out your email-address!');
  }
}
