import { WorkExperience } from './work-experience';
import { Technology } from './technology';
import { Project } from './../_interfaces/project';
export interface Profile {
  id: string; //profile doc id
  uniqueName: string;
  displayName: string;
  userDocId: string;
  isRemote: boolean;
  currentRole?: string;
  country?: string;
  city?: string;
  aboutMe?: string;
  projects?: Project[];
  profilePicturePath?: string;
  profileBackgroundPath?: string;
  primaryTechnologies?: Technology[];
  secondaryTechnologies?: Technology[];
  workExperiences?: WorkExperience[];
  github?: string;
  twitter?: string;
  linkedin?: string;
  stackoverflow?: string;
}
