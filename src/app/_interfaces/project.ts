import { Image } from './../_interfaces/image';
export interface Project {
  name: string;
  technologies: string[];
  summary: string;
  previewColor?: string; //'white' or '#222b45'
  overview?: string;
  previewImage?: Image;
  images?: Image[];
  company?: string;
  publicUrl?: string;
  githubRepository?: string;
  repl?: string;
}
