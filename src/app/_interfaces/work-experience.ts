export interface WorkExperience {
  companyName: string;
  position: string;
  description: string;
  startDateSeconds: number;
  endDateSeconds?: number;
  isCurrent: boolean;
}
