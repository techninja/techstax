/*
Devicon Dictionary

usage: if adding a custom (font)size always add a class for font-size aswell as a class for custom-icons with width and height (see below)

how to add custom icons:
- add entry in icons array with iconName "<name>-icon custom-icon" (default size for icons will be 15px x 15px)
- add svg icon in /assets/icons
- add class entry in custom-icons.scss
- change size in respective component-stylesheet and add class combination like this:
    .preview-tech-icon .custom-icon {
        width: 30px;
        height: 30px;
    }
*/

export interface Devicon {
  iconName: string;
  displayName: string;
}

export const icons: { [techName: string]: Devicon } = {
  aws: { iconName: 'devicon-amazonwebservices-original colored', displayName: 'AWS' },
  android: { iconName: 'devicon-android-plain colored', displayName: 'Android' },
  angular: { iconName: 'devicon-angularjs-plain colored', displayName: 'Angular' },
  apache: { iconName: 'devicon-apache-plain colored', displayName: 'Apache' },
  appcelerator: { iconName: 'devicon-appcelerator-original colored', displayName: 'Appcelerator' },
  ios: { iconName: 'devicon-apple-original colored', displayName: 'iOS' },
  babel: { iconName: 'devicon-babel-plain colored', displayName: 'Babel' },
  backbonejs: { iconName: 'devicon-backbonejs-plain colored', displayName: 'BackboneJS' },
  bootstrap: { iconName: 'devicon-bootstrap-plain colored', displayName: 'Bootstrap' },
  bower: { iconName: 'devicon-bower-plain colored', displayName: 'Bower' },
  c: { iconName: 'devicon-c-plain colored', displayName: 'C' },
  codeigniter: { iconName: 'devicon-codeigniter-plain colored', displayName: 'Codeigniter' },
  coffeescript: { iconName: 'devicon-coffeescript-original colored', displayName: 'CoffeeScript' },
  confluence: { iconName: 'devicon-confluence-plain colored', displayName: 'Confluence' },
  'c++': { iconName: 'devicon-cplusplus-plain colored', displayName: 'C++' },
  'c#': { iconName: 'devicon-csharp-plain colored', displayName: 'C#' },
  css: { iconName: 'devicon-css3-plain colored', displayName: 'CSS' },
  cucumber: { iconName: 'devicon-cucumber-plain colored', displayName: 'Cucumber' },
  d3js: { iconName: 'devicon-d3js-plain colored', displayName: 'D3JS' },
  debian: { iconName: 'devicon-debian-plain colored', displayName: 'Debian' },
  django: { iconName: 'devicon-django-plain colored', displayName: 'Django' },
  docker: { iconName: 'devicon-docker-plain colored', displayName: 'Docker' },
  doctrine: { iconName: 'devicon-doctrine-plain colored', displayName: 'Doctrine' },
  '.net': { iconName: 'devicon-dot-net-plain colored', displayName: '.NET' },
  drupal: { iconName: 'devicon-drupal-plain colored', displayName: 'Drupal' },
  erlang: { iconName: 'devicon-erlang-plain colored', displayName: 'Erlang' },
  express: { iconName: 'devicon-express-original colored', displayName: 'Express' },
  foundation: { iconName: 'devicon-foundation-plain colored', displayName: 'Foundation' },
  gatling: { iconName: 'devicon-gatling-plain colored', displayName: 'Gatling' },
  git: { iconName: 'devicon-git-plain colored', displayName: 'Git' },
  go: { iconName: 'devicon-go-plain colored', displayName: 'Go' },
  gradle: { iconName: 'devicon-gradle-plain colored', displayName: 'Gradle' },
  grunt: { iconName: 'devicon-grunt-plain colored', displayName: 'Grunt' },
  gulp: { iconName: 'devicon-gulp-plain colored', displayName: 'Gulp' },
  heroku: { iconName: 'devicon-heroku-original colored', displayName: 'Heroku' },
  html: { iconName: 'devicon-html5-plain colored', displayName: 'HTML' },
  java: { iconName: 'devicon-java-plain colored', displayName: 'Java' },
  jasmine: { iconName: 'devicon-jasmine-plain colored', displayName: 'Jasmine' },
  javascript: { iconName: 'devicon-javascript-plain colored', displayName: 'JavaScript' },
  jeet: { iconName: 'devicon-jeet-plain colored', displayName: 'Jeet' },
  jquery: { iconName: 'devicon-jquery-plain colored', displayName: 'jQuery' },
  krakenjs: { iconName: 'devicon-krakenjs-plain colored', displayName: 'KrakenJS' },
  laravel: { iconName: 'devicon-laravel-plain colored', displayName: 'Laravel' },
  less: { iconName: 'devicon-less-plain-wordmark colored', displayName: 'Less' },
  linux: { iconName: 'devicon-linux-plain colored', displayName: 'Linux' },
  meteor: { iconName: 'devicon-meteor-plain colored', displayName: 'Meteor' },
  mocha: { iconName: 'devicon-mocha-plain colored', displayName: 'Mocha' },
  mongodb: { iconName: 'devicon-mongodb-plain colored', displayName: 'MongoDB' },
  moodle: { iconName: 'devicon-moodle-plain colored', displayName: 'Moodle' },
  mysql: { iconName: 'devicon-mysql-plain colored', displayName: 'MySQL' },
  nginx: { iconName: 'devicon-nginx-original colored', displayName: 'NGINX' },
  nodejs: { iconName: 'devicon-nodejs-plain colored', displayName: 'NodeJS' },
  nodewebkit: { iconName: 'devicon-nodewebkit-plain colored', displayName: 'Node-webkit' },
  oracle: { iconName: 'devicon-oracle-plain colored', displayName: 'Oracle' },
  php: { iconName: 'devicon-php-plain colored', displayName: 'PHP' },
  protractor: { iconName: 'devicon-protractor-plain colored', displayName: 'Protractor' },
  postgresql: { iconName: 'devicon-postgresql-plain colored', displayName: 'PostgreSQL' },
  python: { iconName: 'devicon-python-plain colored', displayName: 'Python' },
  rails: { iconName: 'devicon-rails-plain colored', displayName: 'Rails' },
  react: { iconName: 'devicon-react-original colored', displayName: 'React' },
  redis: { iconName: 'devicon-redis-plain colored', displayName: 'Redis' },
  ruby: { iconName: 'devicon-ruby-plain colored', displayName: 'Ruby' },
  sass: { iconName: 'devicon-sass-original colored', displayName: 'Sass' },
  sequelize: { iconName: 'devicon-sequelize-plain colored', displayName: 'Sequelize' },
  slack: { iconName: 'devicon-slack-plain colored', displayName: 'Slack' },
  ssh: { iconName: 'devicon-ssh-plain colored', displayName: 'SSH' },
  swift: { iconName: 'devicon-swift-plain colored', displayName: 'Swift' },
  symfony: { iconName: 'devicon-symfony-original colored', displayName: 'Symfony' },
  tomcat: { iconName: 'devicon-tomcat-line colored', displayName: 'Tomcat' },
  travis: { iconName: 'devicon-travis-plain colored', displayName: 'Travis' },
  typescript: { iconName: 'devicon-typescript-plain colored', displayName: 'TypeScript' },
  ubuntu: { iconName: 'devicon-ubuntu-plain colored', displayName: 'Ubuntu' },
  vim: { iconName: 'devicon-vim-plain colored', displayName: 'Vim' },
  vuejs: { iconName: 'devicon-vuejs-plain colored', displayName: 'VueJS' },
  webpack: { iconName: 'devicon-webpack-plain colored', displayName: 'Webpack' },
  wordpress: { iconName: 'devicon-wordpress-plain colored', displayName: 'WordPress' },
  yii: { iconName: 'devicon-yii-plain colored', displayName: 'Yii' },
  firebase: { iconName: 'firebase-icon custom-icon', displayName: 'Firebase' },
  haskell: { iconName: 'haskell-icon custom-icon', displayName: 'Haskell' },
};
