export interface Technology {
  name: string;
  level: number;
  isFavourite: boolean;
}
